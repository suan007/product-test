export type ProductType = {
    title: string,
    price: number,
    units_in_stock: number
}
