import { Product } from '../models/models'
import { ProductType } from './types'

class ProductService {
  async createProduct (product: ProductType, picture: string) {
    return await Product.create({ ...product, picture })
  }

  async getAllProducts (limit?: number, offset?: number) {
    return await Product.findAll({ limit, offset })
  }

  async getProductById (id: number) {
    return await Product.findAll({ where: { id } })
  }

  async updateProduct (title: string, price: number, units_in_stock: number, id: number) {
    return await Product.update(
      { title, price, units_in_stock },
      { where: { id } }
    )
  }

  async deleteProductById (id: number) {
    return await Product.destroy({ where: { id } })
  }
}

export default new ProductService()
