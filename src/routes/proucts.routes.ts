import { Response, Request, Express } from 'express'
import ProductController from '../controllers/product.controller'

export const routes = (app: Express) => {
  // eslint-disable-next-line no-unused-expressions
  app.post('/products', async (req: Request, res: Response) => {
    const file = (req as any).files.picture
    const product = await ProductController.createProduct(req.body, file)
    res.json(product)
  }),
  app.get('/products', async (req: Request, res: Response) => {
    let { page } = req.query
    page = page || '1'
    const products = await ProductController.getAllProducts(Number(page))
    res.json(products)
  }),
  app.get('/product/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    if (!id) {
      res.boom.notFound()
    }
    const products = await ProductController.getProductById(+id)
    res.json(products)
  }),
  app.put('/products', async (req: Request, res: Response) => {
    const {
      title, price, units_in_stock, id
    } = req.body
    if (!id || !title || !price || !units_in_stock) {
      res.boom.badRequest()
    }
    const product = await ProductController.updateProduct(title, price, units_in_stock, id)
    res.json(product)
  }),
  app.delete('/products/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    if (!id) {
      res.boom.notFound()
    }
    const products = await ProductController.deleteProductById(+id)
    res.json(products)
  })
}
