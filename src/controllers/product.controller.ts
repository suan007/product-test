import config from 'config'
import productService from '../services/product.service'
import fileController from './fileController'
import { ProductType } from '../services/types'

class ProductController {
  async createProduct (product: ProductType, file: any) {
    let fileName = fileController.saveFile(file)
    fileName = fileName || 'no picture'
    return await productService.createProduct(product, fileName)
  }

  async getAllProducts (page: number) {
    const limit = config.get<number>('limit')
    const offset = page * limit - limit
    return await productService.getAllProducts(limit, offset)
  }

  async getProductById (id: number) {
    return await productService.getProductById(id)
  }

  async updateProduct (title: string, price: number, units_in_stock: number, id: number) {
    return await productService.updateProduct(title, price, units_in_stock, id)
  }

  async deleteProductById (id: number) {
    const product: any = await productService.getProductById(id)
    fileController.deleteFile(product[0].dataValues.picture)
    await productService.deleteProductById(+id)
    return product
  }
}

export default new ProductController()
