import path from 'path'
import { v4 } from 'uuid'
import fs from 'fs'

class FileController {
  saveFile (file: any) {
    try {
      const fileName = `${v4()}.jpg`
      const filePath = path.resolve('./src/static', fileName)
      file.mv(filePath)
      return fileName
    } catch (e) {
      console.log(e)
    }
  }

  deleteFile (picture: string) {
    console.log(picture)

    try {
      if (fs.existsSync(`./src/static/${picture}`)) {
        fs.unlinkSync(`./src/static/${picture}`)
      } else {
        console.log('The file does not exist.')
      }
    } catch (err) {
      console.error(err)
    }
  }
}

export default new FileController()
