import config from 'config'
import express from 'express'
import cors from 'cors'
import fileUpload from 'express-fileupload'
import { sequelize } from './db/db'
import { routes } from './routes/proucts.routes'

const port = config.get<number>('port')

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.static('static'))
app.use(fileUpload({}))

const start = async () => {
  try {
    await sequelize.authenticate()
    await sequelize.sync()
    routes(app)
    app.listen(port, () => {
      console.log(`app is runnig on port: ${port}`)
    })
  } catch (error) {
    console.log(error)
  }
}

start()
