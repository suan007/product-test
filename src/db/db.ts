import config from 'config'
import { Sequelize } from 'sequelize'

const port = config.get<number>('dbPort')
const user = config.get<string>('dbUser')
const password = config.get<string>('dbPassword')
const host = config.get<string>('dbHost')
const dbName = config.get<string>('dbName')

export const sequelize = new Sequelize(
  dbName,
  user,
  password,
  {
    dialect: 'postgres',
    host,
    port
  }
)
